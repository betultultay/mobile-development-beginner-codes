package com.example.tryings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.VideoView;

public class MainActivity2 extends AppCompatActivity {

    //Widgetları tanımlama
    private ImageView imageView;
    private Button buttonResim1, buttonResim2, button4;
    private VideoView videoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        //Widgetları sayfaya bağlama
        imageView = findViewById(R.id.imageView);
        buttonResim1 = findViewById(R.id.buttonResim1);
        buttonResim2 = findViewById(R.id.buttonResim2);
        videoView = findViewById(R.id.videoView);
        button4 = findViewById(R.id.button4);

        //Buttona listener ekleme
        buttonResim1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageView.setImageResource(R.drawable.resim1);
                Uri adres = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.video);
                videoView.setVideoURI(adres);
                videoView.start();
            }
        });

        buttonResim2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageView.setImageResource(getResources().getIdentifier(
                        "resim2",
                        "drawable",
                        getPackageName()));
                videoView.stopPlayback();
            }
        });

        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent yeniIntent = new Intent(MainActivity2.this, MainActivity3.class);
                startActivity(yeniIntent);
            }
        });
    }
}