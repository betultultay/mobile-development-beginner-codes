package com.example.tryings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {


    private Switch switch1;
    private ToggleButton toggleButton1;
    private RadioButton radioButton2;
    private CheckBox checkBox2;
    private Button button;
    private  TextView tv1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        switch1 = findViewById(R.id.switch1);
        toggleButton1 = findViewById(R.id.toggleButton1);
        button = findViewById(R.id.button8);
        radioButton2 = findViewById(R.id.radioButton3);
        checkBox2 = findViewById(R.id.checkBox2);
        tv1 = findViewById(R.id.tv1);



        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                {
                    tv1.setText("True");
                }
                else
                    {
                        tv1.setText("False");
                    }
            }
        });

        toggleButton1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                {
                    tv1.setText("True");
                    Toast.makeText(getApplicationContext(), "Toggle değeri değişti.",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    tv1.setText("False");
                }
            }
        });

        checkBox2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                {
                    tv1.setText("True");
                    Toast.makeText(getApplicationContext(), "Checkbox işaretlendi.",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    tv1.setText("False");
                }
            }
        });

        radioButton2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                {
                    tv1.setText("True");
                    Toast.makeText(getApplicationContext(), "Radio Button 2 işaretlendi.",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    tv1.setText("False");
                }
            }
        });


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MainActivity2.class);
                startActivity(intent);
                finish();
            }
        });


    }


}