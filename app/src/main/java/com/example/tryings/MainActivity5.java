package com.example.tryings;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity5 extends AppCompatActivity {

    //Widgetları tanımlama
    private Button button9;
    private Spinner spinner;
    //Liste ve veri adaptörü oluşturma
    private ArrayList<String> ulkeler = new ArrayList<>();
    private ArrayAdapter<String> veriAdaptoru;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main5);

        //Widgetları initialize etme
        button9 = findViewById(R.id.button9);
        spinner = findViewById(R.id.spinner);

        //Ülkeler listesine veri ekleme
        ulkeler.add("Türkiye");
        ulkeler.add("Amerika");
        ulkeler.add("İspanya");
        ulkeler.add("İtalya");
        ulkeler.add("Hindistan");
        ulkeler.add("Güney Kore");
        ulkeler.add("Filipinler");
        ulkeler.add("Endonezya");
        ulkeler.add("Almanya");
        ulkeler.add("Brezilya");

        veriAdaptoru = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1,
                android.R.id.text1,ulkeler);
        spinner.setAdapter(veriAdaptoru);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getApplicationContext(),
                        "Seçilen ülke "+ulkeler.get(i),
                         Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(),
                        "Ülke "+ulkeler.get(spinner.getSelectedItemPosition()),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
}