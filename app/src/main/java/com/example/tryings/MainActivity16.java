package com.example.tryings;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class MainActivity16 extends AppCompatActivity {

    private ImageView imageView01,imageView02,imageView03,imageView04,imageView05,imageView06,imageView07,imageView08,imageView09,imageView10,imageView11
            ,imageView_12,imageView13,imageView14, imageView15,imageView16,imageView17,imageView18
            ,imageView19,imageView20,imageView21,imageView22,imageView23,imageView24;

    Integer[] cards = {101,102,103,104,105,106,107,108,109,110,111,112,201,202,203,204,205,206,207,208,209,210,211,212};
    int image101,image102,image103,image104,image105,image106,image107,image108,image109,image110,image111
    ,image112,image201,image202,image203,image204,image205,image206,image207,image208,image209,image210,image211,image212;
    int firstCard, secondCard;
    int clickedFirst, clickedSecond;
    int cardNumber = 1;
    Handler handler;
    Runnable runnable;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main16);

        imageView01 = findViewById(R.id.imageView01);
        imageView02 = findViewById(R.id.imageView02);
        imageView03 = findViewById(R.id.imageView03);
        imageView04 = findViewById(R.id.imageView04);
        imageView05 = findViewById(R.id.imageView05);
        imageView06 = findViewById(R.id.imageView06);
        imageView07 = findViewById(R.id.imageView07);
        imageView08 = findViewById(R.id.imageView08);
        imageView09 = findViewById(R.id.imageView09);
        imageView10 = findViewById(R.id.imageView10);
        imageView11 = findViewById(R.id.imageView11);
        imageView_12 = findViewById(R.id.imageView_12);
        imageView13 = findViewById(R.id.imageView13);
        imageView14 = findViewById(R.id.imageView14);
        imageView15 = findViewById(R.id.imageView15);
        imageView16 = findViewById(R.id.imageView16);
        imageView17 = findViewById(R.id.imageView17);
        imageView18 = findViewById(R.id.imageView18);
        imageView19 = findViewById(R.id.imageView19);
        imageView20 = findViewById(R.id.imageView20);
        imageView21 = findViewById(R.id.imageView21);
        imageView22 = findViewById(R.id.imageView22);
        imageView23 = findViewById(R.id.imageView23);
        imageView24 = findViewById(R.id.imageView24);

        imageView01.setTag("1");
        imageView02.setTag("2");
        imageView03.setTag("3");
        imageView04.setTag("4");
        imageView05.setTag("5");
        imageView06.setTag("6");
        imageView07.setTag("7");
        imageView08.setTag("8");
        imageView09.setTag("9");
        imageView10.setTag("10");
        imageView11.setTag("11");
        imageView_12.setTag("12");
        imageView13.setTag("13");
        imageView14.setTag("14");
        imageView15.setTag("15");
        imageView16.setTag("16");
        imageView17.setTag("17");
        imageView18.setTag("18");
        imageView19.setTag("19");
        imageView20.setTag("20");
        imageView21.setTag("21");
        imageView22.setTag("22");
        imageView23.setTag("23");
        imageView24.setTag("24");

        frontOfCardResources();

        Collections.shuffle(Arrays.asList(cards));

        imageView01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int theCard = Integer.parseInt((String) view.getTag());
                doStuff(imageView01,theCard);
            }
        });

        imageView02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int theCard = Integer.parseInt((String) view.getTag());
                doStuff(imageView02,theCard);
            }
        });

        imageView03.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int theCard = Integer.parseInt((String) view.getTag());
                doStuff(imageView03,theCard);
            }
        });

        imageView04.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int theCard = Integer.parseInt((String) view.getTag());
                doStuff(imageView04,theCard);
            }
        });

        imageView05.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int theCard = Integer.parseInt((String) view.getTag());
                doStuff(imageView05,theCard);
            }
        });

        imageView06.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int theCard = Integer.parseInt((String) view.getTag());
                doStuff(imageView06,theCard);
            }
        });

        imageView07.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int theCard = Integer.parseInt((String) view.getTag());
                doStuff(imageView07,theCard);
            }
        });

        imageView08.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int theCard = Integer.parseInt((String) view.getTag());
                doStuff(imageView08,theCard);
            }
        });

        imageView09.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int theCard = Integer.parseInt((String) view.getTag());
                doStuff(imageView09,theCard);
            }
        });

        imageView10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int theCard = Integer.parseInt((String) view.getTag());
                doStuff(imageView10,theCard);
            }
        });

        imageView11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int theCard = Integer.parseInt((String) view.getTag());
                doStuff(imageView11,theCard);
            }
        });

        imageView_12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int theCard = Integer.parseInt((String) view.getTag());
                doStuff(imageView_12,theCard);
            }
        });

        imageView13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int theCard = Integer.parseInt((String) view.getTag());
                doStuff(imageView13,theCard);
            }
        });

        imageView14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int theCard = Integer.parseInt((String) view.getTag());
                doStuff(imageView14,theCard);
            }
        });

        imageView15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int theCard = Integer.parseInt((String) view.getTag());
                doStuff(imageView15,theCard);
            }
        });

        imageView16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int theCard = Integer.parseInt((String) view.getTag());
                doStuff(imageView16,theCard);
            }
        });

        imageView17.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int theCard = Integer.parseInt((String) view.getTag());
                doStuff(imageView17,theCard);
            }
        });

        imageView18.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int theCard = Integer.parseInt((String) view.getTag());
                doStuff(imageView18,theCard);
            }
        });

        imageView19.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int theCard = Integer.parseInt((String) view.getTag());
                doStuff(imageView19,theCard);
            }
        });

        imageView20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int theCard = Integer.parseInt((String) view.getTag());
                doStuff(imageView20,theCard);
            }
        });


        imageView21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int theCard = Integer.parseInt((String) view.getTag());
                doStuff(imageView21,theCard);
            }
        });

        imageView22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int theCard = Integer.parseInt((String) view.getTag());
                doStuff(imageView22,theCard);
            }
        });

        imageView23.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int theCard = Integer.parseInt((String) view.getTag());
                doStuff(imageView23,theCard);
            }


        });

        imageView24.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int theCard = Integer.parseInt((String) view.getTag());
                doStuff(imageView24,theCard);
            }


        });

    }

    private void doStuff(ImageView iv, int card) {
        if(cards[card] == 101)
        {
            iv.setImageResource(image101);
        }
        else if(cards[card] == 102)
        {
            iv.setImageResource(image102);
        }
        else if(cards[card] == 103)
        {
            iv.setImageResource(image103);
        }
        else if(cards[card] == 104)
        {
            iv.setImageResource(image104);
        }
        else if(cards[card] == 105)
        {
            iv.setImageResource(image105);
        }
        else if(cards[card] == 106)
        {
            iv.setImageResource(image106);
        }
        else if(cards[card] == 107)
        {
            iv.setImageResource(image107);
        }
        else if(cards[card] == 108)
        {
            iv.setImageResource(image108);
        }
        else if(cards[card] == 109)
        {
            iv.setImageResource(image109);
        }
        else if(cards[card] == 110)
        {
            iv.setImageResource(image110);
        }
        else if(cards[card] == 111)
        {
            iv.setImageResource(image111);
        }
        else if(cards[card] == 112)
        {
            iv.setImageResource(image112);
        }
        else if(cards[card] == 201)
        {
            iv.setImageResource(image201);
        }
        else if(cards[card] == 202)
        {
            iv.setImageResource(image202);
        }
        else if(cards[card] == 203)
        {
            iv.setImageResource(image203);
        }
        else if(cards[card] == 204)
        {
            iv.setImageResource(image204);
        }
        else if(cards[card] == 205)
        {
            iv.setImageResource(image205);
        }
        else if(cards[card] == 206)
        {
            iv.setImageResource(image206);
        }
        else if(cards[card] == 207)
        {
            iv.setImageResource(image207);
        }
        else if(cards[card] == 208)
        {
            iv.setImageResource(image208);
        }
        else if(cards[card] == 209)
        {
            iv.setImageResource(image209);
        }
        else if(cards[card] == 210)
        {
            iv.setImageResource(image210);
        }
        else if(cards[card] == 211)
        {
            iv.setImageResource(image211);
        }
        else if(cards[card] == 212)
        {
            iv.setImageResource(image212);
        }

        if(cardNumber == 1)
        {
            firstCard = cards[card];
            if(firstCard > 200)
            {
                firstCard = firstCard - 100;
            }
            cardNumber = 2;
            clickedFirst = card;
            iv.setEnabled(false);
        }
        else if(cardNumber == 2)
        {
            secondCard = cards[card];
            if(secondCard > 200)
            {
                secondCard = secondCard - 100;
            }
            cardNumber = 1;
            clickedSecond = card;

            handler =new Handler() ;
            handler.postDelayed(new Runnable() {
                public void run() {
                   calculate();
                }

            }, 2000);
        }

    }

    private void calculate()
    {
        if(firstCard == secondCard)
        {
            if(clickedFirst == 1)
            {imageView01.setVisibility(View.INVISIBLE); }
            else if(clickedFirst == 2)
            { imageView02.setVisibility(View.INVISIBLE);}
            else if(clickedFirst == 3)
            {imageView03.setVisibility(View.INVISIBLE); }
            else if(clickedFirst == 4)
            { imageView04.setVisibility(View.INVISIBLE);}
            else if(clickedFirst == 5)
            { imageView05.setVisibility(View.INVISIBLE);}
            else if(clickedFirst == 6)
            { imageView06.setVisibility(View.INVISIBLE); }
            else if(clickedFirst == 7)
            { imageView07.setVisibility(View.INVISIBLE);}
            else if(clickedFirst == 8)
            { imageView08.setVisibility(View.INVISIBLE);}
            else if(clickedFirst == 9)
            { imageView09.setVisibility(View.INVISIBLE);}
            else if(clickedFirst == 10)
            { imageView10.setVisibility(View.INVISIBLE);}
            else if(clickedFirst == 11)
            { imageView11.setVisibility(View.INVISIBLE);}
            else if(clickedFirst == 12)
            { imageView_12.setVisibility(View.INVISIBLE);}
            else if(clickedFirst == 13)
            { imageView13.setVisibility(View.INVISIBLE); }
            else if(clickedFirst == 14)
            { imageView14.setVisibility(View.INVISIBLE);}
            else if(clickedFirst == 15)
            { imageView15.setVisibility(View.INVISIBLE);}
            else if(clickedFirst == 16)
            { imageView16.setVisibility(View.INVISIBLE);}
            else if(clickedFirst == 17)
            { imageView17.setVisibility(View.INVISIBLE);}
            else if(clickedFirst == 18)
            { imageView18.setVisibility(View.INVISIBLE);}
            else if(clickedFirst == 19)
            { imageView19.setVisibility(View.INVISIBLE);}
            else if(clickedFirst == 20)
            { imageView20.setVisibility(View.INVISIBLE);}
            else if(clickedFirst == 21)
            { imageView21.setVisibility(View.INVISIBLE);}
            else if(clickedFirst == 22)
            { imageView22.setVisibility(View.INVISIBLE);}
            else if(clickedFirst == 23)
            { imageView23.setVisibility(View.INVISIBLE);}
            else if(clickedFirst == 24)
            { imageView24.setVisibility(View.INVISIBLE);}


            if(clickedSecond == 1)
            {imageView01.setVisibility(View.INVISIBLE); }
            else if(clickedSecond == 2)
            { imageView02.setVisibility(View.INVISIBLE);}
            else if(clickedSecond == 3)
            {imageView03.setVisibility(View.INVISIBLE); }
            else if(clickedSecond == 4)
            { imageView04.setVisibility(View.INVISIBLE);}
            else if(clickedSecond == 5)
            { imageView05.setVisibility(View.INVISIBLE);}
            else if(clickedSecond == 6)
            { imageView06.setVisibility(View.INVISIBLE); }
            else if(clickedSecond == 7)
            { imageView07.setVisibility(View.INVISIBLE);}
            else if(clickedSecond == 8)
            { imageView08.setVisibility(View.INVISIBLE);}
            else if(clickedSecond == 9)
            { imageView09.setVisibility(View.INVISIBLE);}
            else if(clickedSecond == 10)
            { imageView10.setVisibility(View.INVISIBLE);}
            else if(clickedSecond == 11)
            { imageView11.setVisibility(View.INVISIBLE);}
            else if(clickedSecond == 12)
            { imageView_12.setVisibility(View.INVISIBLE);}
            else if(clickedSecond == 13)
            { imageView13.setVisibility(View.INVISIBLE); }
            else if(clickedSecond == 14)
            { imageView14.setVisibility(View.INVISIBLE);}
            else if(clickedSecond == 15)
            { imageView15.setVisibility(View.INVISIBLE);}
            else if(clickedSecond == 16)
            { imageView16.setVisibility(View.INVISIBLE);}
            else if(clickedSecond == 17)
            { imageView17.setVisibility(View.INVISIBLE);}
            else if(clickedSecond == 18)
            { imageView18.setVisibility(View.INVISIBLE);}
            else if(clickedSecond == 19)
            { imageView19.setVisibility(View.INVISIBLE);}
            else if(clickedSecond == 20)
            { imageView20.setVisibility(View.INVISIBLE);}
            else if(clickedSecond == 21)
            { imageView21.setVisibility(View.INVISIBLE);}
            else if(clickedSecond == 22)
            { imageView22.setVisibility(View.INVISIBLE);}
            else if(clickedSecond == 23)
            { imageView23.setVisibility(View.INVISIBLE);}
            else if(clickedSecond == 24)
            { imageView24.setVisibility(View.INVISIBLE);}

        }
        else
        {
            if(clickedFirst == 1)
            {imageView01.setImageResource(R.drawable.bharfi); }
            else if(clickedFirst == 2)
            { imageView02.setImageResource(R.drawable.bharfi);}
            else if(clickedFirst == 3)
            {imageView03.setImageResource(R.drawable.bharfi); }
            else if(clickedFirst == 4)
            { imageView04.setImageResource(R.drawable.bharfi);}
            else if(clickedFirst == 5)
            { imageView05.setImageResource(R.drawable.bharfi);}
            else if(clickedFirst == 6)
            { imageView06.setImageResource(R.drawable.bharfi); }
            else if(clickedFirst == 7)
            { imageView07.setImageResource(R.drawable.bharfi);}
            else if(clickedFirst == 8)
            { imageView08.setImageResource(R.drawable.bharfi);}
            else if(clickedFirst == 9)
            { imageView09.setImageResource(R.drawable.bharfi);}
            else if(clickedFirst == 10)
            { imageView10.setImageResource(R.drawable.bharfi);}
            else if(clickedFirst == 11)
            { imageView11.setImageResource(R.drawable.bharfi);}
            else if(clickedFirst == 12)
            { imageView_12.setImageResource(R.drawable.bharfi);}
            else if(clickedFirst == 13)
            { imageView13.setImageResource(R.drawable.bharfi); }
            else if(clickedFirst == 14)
            { imageView14.setImageResource(R.drawable.bharfi);}
            else if(clickedFirst == 15)
            { imageView15.setImageResource(R.drawable.bharfi);}
            else if(clickedFirst == 16)
            { imageView16.setImageResource(R.drawable.bharfi);}
            else if(clickedFirst == 17)
            { imageView17.setImageResource(R.drawable.bharfi);}
            else if(clickedFirst == 18)
            { imageView18.setImageResource(R.drawable.bharfi);}
            else if(clickedFirst == 19)
            { imageView19.setImageResource(R.drawable.bharfi);}
            else if(clickedFirst == 20)
            { imageView20.setImageResource(R.drawable.bharfi);}
            else if(clickedFirst == 21)
            { imageView21.setImageResource(R.drawable.bharfi);}
            else if(clickedFirst == 22)
            { imageView22.setImageResource(R.drawable.bharfi);}
            else if(clickedFirst == 23)
            { imageView23.setImageResource(R.drawable.bharfi);}
            else if(clickedFirst == 24)
            { imageView23.setImageResource(R.drawable.bharfi);}


            if(clickedSecond == 1)
            {imageView01.setImageResource(R.drawable.bharfi); }
            else if(clickedSecond == 2)
            { imageView02.setImageResource(R.drawable.bharfi);}
            else if(clickedSecond == 3)
            {imageView03.setImageResource(R.drawable.bharfi);}
            else if(clickedSecond == 4)
            { imageView04.setImageResource(R.drawable.bharfi);}
            else if(clickedSecond == 5)
            { imageView05.setImageResource(R.drawable.bharfi);}
            else if(clickedSecond == 6)
            { imageView06.setImageResource(R.drawable.bharfi); }
            else if(clickedSecond == 7)
            { imageView07.setImageResource(R.drawable.bharfi);}
            else if(clickedSecond == 8)
            { imageView08.setImageResource(R.drawable.bharfi);}
            else if(clickedSecond == 9)
            { imageView09.setImageResource(R.drawable.bharfi);}
            else if(clickedSecond == 10)
            { imageView10.setImageResource(R.drawable.bharfi);}
            else if(clickedSecond == 11)
            { imageView11.setImageResource(R.drawable.bharfi);}
            else if(clickedSecond == 12)
            { imageView_12.setImageResource(R.drawable.bharfi);}
            else if(clickedSecond == 13)
            { imageView13.setImageResource(R.drawable.bharfi); }
            else if(clickedSecond == 14)
            { imageView14.setImageResource(R.drawable.bharfi);}
            else if(clickedSecond == 15)
            { imageView15.setImageResource(R.drawable.bharfi);}
            else if(clickedSecond == 16)
            { imageView16.setImageResource(R.drawable.bharfi);}
            else if(clickedSecond == 17)
            { imageView17.setImageResource(R.drawable.bharfi);}
            else if(clickedSecond == 18)
            { imageView18.setImageResource(R.drawable.bharfi);}
            else if(clickedSecond == 19)
            { imageView19.setImageResource(R.drawable.bharfi);}
            else if(clickedSecond == 20)
            { imageView20.setImageResource(R.drawable.bharfi);}
            else if(clickedSecond == 21)
            { imageView21.setImageResource(R.drawable.bharfi);}
            else if(clickedSecond == 22)
            { imageView22.setImageResource(R.drawable.bharfi);}
            else if(clickedSecond == 23)
            { imageView23.setImageResource(R.drawable.bharfi);}
            else if(clickedSecond == 24)
            { imageView24.setImageResource(R.drawable.bharfi);}

        }
        checkEnd();
    }

    private void checkEnd()
    {
        if(imageView01.getVisibility() == View.INVISIBLE &&
                imageView01.getVisibility() == View.INVISIBLE &&
                imageView02.getVisibility() == View.INVISIBLE &&
                imageView03.getVisibility() == View.INVISIBLE &&
                imageView04.getVisibility() == View.INVISIBLE &&
                imageView05.getVisibility() == View.INVISIBLE &&
                imageView06.getVisibility() == View.INVISIBLE &&
                imageView07.getVisibility() == View.INVISIBLE &&
                imageView08.getVisibility() == View.INVISIBLE &&
                imageView09.getVisibility() == View.INVISIBLE &&
                imageView10.getVisibility() == View.INVISIBLE &&
                imageView11.getVisibility() == View.INVISIBLE &&
                imageView_12.getVisibility() == View.INVISIBLE &&
                imageView13.getVisibility() == View.INVISIBLE &&
                imageView14.getVisibility() == View.INVISIBLE &&
                imageView15.getVisibility() == View.INVISIBLE &&
                imageView16.getVisibility() == View.INVISIBLE &&
                imageView17.getVisibility() == View.INVISIBLE &&
                imageView18.getVisibility() == View.INVISIBLE &&
                imageView19.getVisibility() == View.INVISIBLE &&
                imageView20.getVisibility() == View.INVISIBLE &&
                imageView21.getVisibility() == View.INVISIBLE &&
                imageView22.getVisibility() == View.INVISIBLE &&
                imageView23.getVisibility() == View.INVISIBLE &&
                imageView24.getVisibility() == View.INVISIBLE )
        {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity16.this);
            alertDialog.setMessage("Oyun bitti.");
            alertDialog.setCancelable(false);
            alertDialog.setPositiveButton("Yeniden oyna.", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent = new Intent(getApplicationContext(),MainActivity16.class);
                    startActivity(intent);
                    finish();
                }
            })
                    .setNegativeButton("Oyundan çık.", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    });
            AlertDialog alertDialog1 = alertDialog.create();
            alertDialog1.show();

        }
    }

    private void frontOfCardResources() {
        image101 = R.drawable.image101;
        image102 = R.drawable.image102;
        image103 = R.drawable.image103;
        image104 = R.drawable.image104;
        image105 = R.drawable.image105;
        image106 = R.drawable.image106;
        image107 = R.drawable.image107;
        image108 = R.drawable.image108;
        image109 = R.drawable.image109;
        image110 = R.drawable.image110;
        image111 = R.drawable.image111;
        image112 = R.drawable.image112;
        image201 = R.drawable.image201;
        image202 = R.drawable.image202;
        image203 = R.drawable.image203;
        image204 = R.drawable.image204;
        image205 = R.drawable.image205;
        image206 = R.drawable.image206;
        image207 = R.drawable.image207;
        image208 = R.drawable.image208;
        image209 = R.drawable.image209;
        image210 = R.drawable.image210;
        image211 = R.drawable.image211;
        image212 = R.drawable.image212;




    }
}