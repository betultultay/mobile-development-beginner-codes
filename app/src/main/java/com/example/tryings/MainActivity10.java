package com.example.tryings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity10 extends AppCompatActivity {

    private Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main10);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Toolbar başlığı");
        toolbar.setSubtitle("Toolbar alt bağlığı");
        toolbar.setLogo(R.drawable.bharfikucuk);
        toolbar.setTitleTextColor(getResources().getColor(R.color.design_default_color_primary_dark));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbarmenu,menu);

        return true;

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.itemDuzenle:
                Toast.makeText(getApplicationContext(), "Düzenle tıklandı.",Toast.LENGTH_SHORT).show();
                return true;

            case R.id.itemInfo:
                Toast.makeText(getApplicationContext(), "Bilgi tıklandı.",Toast.LENGTH_SHORT).show();
                return true;

            case R.id.itemKopyala:
                Toast.makeText(getApplicationContext(), "Kopyala tıklandı.",Toast.LENGTH_SHORT).show();
                return true;

            case R.id.itemSil:
                Toast.makeText(getApplicationContext(), "Sil tıklandı.",Toast.LENGTH_SHORT).show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }


    }
}