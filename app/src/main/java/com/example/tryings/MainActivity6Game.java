package com.example.tryings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class MainActivity6Game extends AppCompatActivity {


    //Widgetları tanımlama
    private Button btnTahminEt;
    private EditText txtSayiGirisi;
    private TextView tvArtırAzalt, tvKalanHak;
    private int random, sayac = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main6_game);

        //Widgetları initialize etme
        btnTahminEt = findViewById(R.id.btnTahminEt);
        txtSayiGirisi = findViewById(R.id.txtSayiGirisi);
        tvArtırAzalt = findViewById(R.id.tvArtırAzalt);
        tvKalanHak = findViewById(R.id.tvKalanHak);

        //Rastgele Sayı Üretme
        Random r = new Random();
        random = r.nextInt(101); //0-100
        Log.e("Sayı", String.valueOf(random));

        btnTahminEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sayac -=1;
                int tahminiSayi = Integer.parseInt(txtSayiGirisi.getText().toString());
                if (sayac != 0)
                {
                    if(tahminiSayi == random)
                    {
                        Intent i = new Intent(MainActivity6Game.this, MainActivity7Game.class);
                        i.putExtra("sonuc", true);
                        startActivity(i);
                        finish();
                        return;
                    }
                    else if(tahminiSayi > random)
                    {
                        tvArtırAzalt.setText("Azalt");
                        tvKalanHak.setText("Kalan Hak: "+sayac);
                    }
                    else if(tahminiSayi < random)
                    {
                        tvArtırAzalt.setText("Artır");
                        tvKalanHak.setText("Kalan Hak: "+sayac);
                    }
                }
                else
                {
                    Intent i = new Intent(MainActivity6Game.this, MainActivity7Game.class);
                    i.putExtra("sonuc", false);
                    startActivity(i);
                    finish();

                }
                txtSayiGirisi.setText("");
            }
        });



    }
}