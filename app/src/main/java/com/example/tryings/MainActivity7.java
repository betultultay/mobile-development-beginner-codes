package com.example.tryings;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity7 extends AppCompatActivity {

    private Button normalAlert, ozelAlert;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main7);

        normalAlert = findViewById(R.id.normalAlert);
        ozelAlert = findViewById(R.id.ozelAlert);

        normalAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity7.this);

                alertDialog.setMessage("Mesaj metni");
                alertDialog.setTitle("Mesaj başlığı");
                alertDialog.setIcon(R.drawable.bharfi);

                alertDialog.setPositiveButton("Tamam", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(getApplicationContext(),"Tamam tıklandı.",Toast.LENGTH_SHORT).show();
                    }
                });

                alertDialog.setNegativeButton("İptal", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(getApplicationContext(),"İptal tıklandı.",Toast.LENGTH_SHORT).show();
                    }
                });
                alertDialog.create().show();
            }
        });

        ozelAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              View tasarim = getLayoutInflater().inflate(R.layout.alert_tasarim,null);
                final EditText txtAlert = tasarim.findViewById(R.id.txtAlert);
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity7.this);
                alertDialog.setMessage("Mesaj");
                alertDialog.setView(tasarim);
                alertDialog.setPositiveButton("Kaydet", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String gelenVeri = txtAlert.getText().toString();
                        Toast.makeText(getApplicationContext(),gelenVeri,Toast.LENGTH_SHORT).show();
                    }
                });

                alertDialog.setNegativeButton("İptal", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String gelenVeri = txtAlert.getText().toString();
                        Toast.makeText(getApplicationContext(),"Özel alert iptal edildi.",Toast.LENGTH_SHORT).show();
                    }
                });

                alertDialog.create().show();
            }
        });
    }
}