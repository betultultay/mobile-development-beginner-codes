package com.example.tryings;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity14 extends AppCompatActivity {

    private ImageView ivPc, ivOyuncu;
    private TextView tvPc, tvOyuncu;
    private Button btnTas, btnKagit, btnMakas;
    private String oyuncuSecimi,os;
    private int sayacPc=0,sayacOyuncu=0;
    Handler handler;
    Runnable runnable;
    private String move;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main14);

        ivPc = findViewById(R.id.ivPc);
        ivOyuncu = findViewById(R.id.ivOyuncu);
        tvPc = findViewById(R.id.tvPc);
        tvOyuncu = findViewById(R.id.tvOyuncu);
        btnTas = findViewById(R.id.btnTas);
        btnKagit = findViewById(R.id.btnKagit);
        btnMakas = findViewById(R.id.btnMakas);
        ivOyuncu.setVisibility(View.INVISIBLE);
        ivPc.setVisibility(View.INVISIBLE);

        btnTas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            ivOyuncu.setImageResource(R.drawable.rock);
            ivOyuncu.setVisibility(View.VISIBLE);
            oyuncuSecimi = "Taş";
            CompareAndResult(oyuncuSecimi);

            }
        });

        btnKagit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            ivOyuncu.setImageResource(R.drawable.paper);
                ivOyuncu.setVisibility(View.VISIBLE);
                oyuncuSecimi = "Kağıt";
                CompareAndResult(oyuncuSecimi);
            }
        });

        btnMakas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            ivOyuncu.setImageResource(R.drawable.scissors);
                ivOyuncu.setVisibility(View.VISIBLE);
                oyuncuSecimi = "Makas";
                CompareAndResult(oyuncuSecimi);
            }
        });

    }
    
    private void CompareAndResult(String oyuncuSecimi) {
        os = oyuncuSecimi;
        handler =new Handler() ;
        handler.postDelayed(new Runnable() {
            public void run() {

        String pcSecimi="";
        Random r = new Random();
        int number = r.nextInt(3)+1;

        if(number == 1)
        {
            pcSecimi = "Taş";

            ivPc.setImageResource(R.drawable.rock);
            ivPc.setVisibility(View.VISIBLE);
        }
        else if(number == 2)
        {
            pcSecimi = "Kağıt";
            ivPc.setImageResource(R.drawable.paper);
            ivPc.setVisibility(View.VISIBLE);
        }
        else if(number == 3)
        {
            pcSecimi = "Makas";
            ivPc.setImageResource(R.drawable.scissors);
            ivPc.setVisibility(View.VISIBLE);
        }

        if(pcSecimi == os)
        {
            Toast.makeText(getApplicationContext(),"Berabere",Toast.LENGTH_SHORT).show();
            tvOyuncu.setText("Skorunuz: "+sayacOyuncu);
            tvPc.setText("Skor: "+sayacPc);
        }

        else if(pcSecimi == "Taş" && os == "Kağıt")
        {
            Toast.makeText(getApplicationContext(),"Kazandınız.",Toast.LENGTH_SHORT).show();
            sayacOyuncu ++;
            tvOyuncu.setText("Skorunuz: "+sayacOyuncu);
        }
        else if(pcSecimi == "Taş" && os == "Makas")
        {
            Toast.makeText(getApplicationContext(),"Kaybettiniz.",Toast.LENGTH_SHORT).show();
            sayacPc ++;
            tvPc.setText("Skor: "+sayacPc);
        }

        else if(pcSecimi == "Kağıt" && os == "Taş")
        {
            Toast.makeText(getApplicationContext(),"Kaybettiniz.",Toast.LENGTH_SHORT).show();
            sayacPc ++;
            tvPc.setText("Skor: "+sayacPc);
        }

        else if(pcSecimi == "Kağıt" && os == "Makas")
        {
            Toast.makeText(getApplicationContext(),"Kazandınız.",Toast.LENGTH_SHORT).show();
            sayacOyuncu ++;
            tvOyuncu.setText("Skorunuz: "+sayacOyuncu);
        }

        else if(pcSecimi == "Makas" && os == "Taş")
        {
            Toast.makeText(getApplicationContext(),"Kazandınız.",Toast.LENGTH_SHORT).show();
            sayacOyuncu ++;
            tvOyuncu.setText("Skorunuz: "+sayacOyuncu);
        }

        else if(pcSecimi == "Makas" && os == "Kağıt")
        {
            Toast.makeText(getApplicationContext(),"Kaybettiniz.",Toast.LENGTH_SHORT).show();
            sayacPc ++;
            tvPc.setText("Skor: "+sayacPc);
        }

            }

        }, 2000);
    }
}