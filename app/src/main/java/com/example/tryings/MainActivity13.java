package com.example.tryings;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity13 extends AppCompatActivity {

    private RecyclerView rv;
    private ArrayList<Film> filmArrayList;
    private FilmAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main13);
        rv = findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL));

        Film film = new Film(1,"Django","a",14.99);
        Film film2 = new Film(2,"Interstellar","b",9.99);
        Film film3 = new Film(3,"Inception","c",12.99);
        Film film4 = new Film(4,"Titanic","d",24.99);
        Film film5 = new Film(5,"Zorro","e",15.99);
        Film film6 = new Film(6,"The Pianist","f",12.99);
        Film film7 = new Film(7,"Beautiful Mind","g",29.99);
        Film film8 = new Film(8,"Jumanji","h",14.99);
        filmArrayList = new ArrayList<>();
        filmArrayList.add(film);
        filmArrayList.add(film2);
        filmArrayList.add(film3);
        filmArrayList.add(film4);
        filmArrayList.add(film5);
        filmArrayList.add(film6);
        filmArrayList.add(film7);
        filmArrayList.add(film8);

        adapter = new FilmAdapter(this, filmArrayList);
        rv.setAdapter(adapter);

    }
}