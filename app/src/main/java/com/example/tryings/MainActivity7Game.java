package com.example.tryings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity7Game extends AppCompatActivity {

    private Button btnTekrarOyna;
    private ImageView imageViewSonuc;
    private TextView tvSonuc;
    private Boolean sonuc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity7_game);

        btnTekrarOyna = findViewById(R.id.btnTekrarOyna);
        imageViewSonuc = findViewById(R.id.imageViewSonuc);
        tvSonuc = findViewById(R.id.tvSonuc);

         sonuc = getIntent().getBooleanExtra("sonuc",false);
        if(sonuc)
        {
            imageViewSonuc.setImageResource(R.drawable.win);
            tvSonuc.setText("Kazandınız.");
        }
        else
        {
            imageViewSonuc.setImageResource(R.drawable.lost);
            tvSonuc.setText("Kaybettiniz.");
        }

        btnTekrarOyna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent yeniIntent = new Intent(MainActivity7Game.this, MainActivity6Game.class);
                startActivity(yeniIntent);
                finish();
            }
        });
    }
}