package com.example.tryings;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity15 extends AppCompatActivity implements View.OnClickListener {

    private Button[][] buttons = new Button[3][3];
    private Boolean player1Turn = true;
    private int roundcount;
    private int player1point;
    private int player2point;
    private TextView textViewPlayer1;
    private TextView textViewPlayer2;
    private Button buttonReset;
    Handler handler;
    Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main15);

        textViewPlayer1 = findViewById(R.id.textView2);
        textViewPlayer2 = findViewById(R.id.textView3);
        buttonReset = findViewById(R.id.button21);
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
                String buttonID = ("button_"+i+j);
                int realID = getResources().getIdentifier(buttonID,"id", getPackageName());
                buttons[i][j] = findViewById(realID);
                buttons[i][j].setOnClickListener(this);
            }
        }
        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                player1point = 0;
                player2point = 0;
                roundcount = 0;
                textViewPlayer1.setText("Oyuncu 1: "+player1point);
                textViewPlayer2.setText("Oyuncu 2: "+player2point);


            }
        });

    }

    @Override
    public void onClick(View view) {
        if(!((Button) view).getText().toString().equals(""))
        {
            return;
        }

        if(player1Turn)
        { ((Button) view).setText("X");}
        else
        { ((Button) view).setText("O");}

        roundcount++;
        if(checkForWin())
        {
            if(player1Turn)
            {player1Wins();}
            else
            {player2Wins();}

        }
        else if(roundcount == 9)
        {
            draw();
        }
        else
        {
            player1Turn = !player1Turn;
        }
    }

    private void player1Wins()
    {
        player1point++;
        Toast.makeText(getApplicationContext(), "1. oyuncu kazandı.",Toast.LENGTH_SHORT).show();
        updatePointsText();
        resetBoard();
    }

    private void player2Wins()
    {
        player2point++;
        Toast.makeText(getApplicationContext(), "2. oyuncu kazandı.",Toast.LENGTH_SHORT).show();
        updatePointsText();

        resetBoard();
    }

    private void draw()
    {
        Toast.makeText(getApplicationContext(), "Çiz!",Toast.LENGTH_SHORT).show();
        resetBoard();

    }
    private void updatePointsText()
    {
      textViewPlayer1.setText("Oyuncu 1: "+player1point);
      textViewPlayer2.setText("Oyuncu 2: "+player2point);


    }
    private void resetBoard()
    {

         handler =new Handler() ;
        handler.postDelayed(new Runnable() {
            public void run() {
                //put your code here
                for(int i=0;i<3;i++)
                {
                    for(int j=0;j<3;j++)
                    {
                        buttons[i][j].setText("");
                    }
                }

                roundcount = 0;
                player1Turn = true;

            }

        }, 2000);
    }


    private Boolean checkForWin()
    {
        String[][] field = new String[3][3];
        for(int i=0;i<3;i++)
        {
            for(int j=0;j<3;j++)
            {
               field[i][j] = buttons[i][j].getText().toString();
            }
        }

        for(int i=0;i<3;i++)
        {
            if(field[i][0].equals(field[i][1])
               && field[i][0].equals(field[i][2])
               && !field[i][0].equals(""))
            {
                return true;
            }
        }

        for(int i=0;i<3;i++)
        {
            if(field[0][i].equals(field[1][i])
                    && field[0][i].equals(field[2][i])
                    && !field[0][i].equals(""))
            {
                return true;
            }
        }
        for(int i=0;i<3;i++)
        {
            if(field[0][0].equals(field[1][1])
                    && field[0][0].equals(field[2][2])
                    && !field[0][0].equals(""))
            {
                return true;
            }
        }

        for(int i=0;i<3;i++)
        {
            if(field[0][2].equals(field[1][1])
                    && field[0][2].equals(field[2][0])
                    && !field[0][2].equals(""))
            {
                return true;
            }
        }
        return false;
    }
}