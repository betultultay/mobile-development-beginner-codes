package com.example.tryings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity3 extends AppCompatActivity {

    private Button button1, button2, button3, button4, button5, button6, button7, button8, button14, button15, button16, button17, button18, button19, button20, button22;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        button1 = findViewById((R.id.button1));
        button2 = findViewById((R.id.button2));
        button3 = findViewById((R.id.button3));
        button4 = findViewById((R.id.button4));
        button5 = findViewById((R.id.button5));
        button6 = findViewById((R.id.button6));
        button7 = findViewById((R.id.button7));
        button8 = findViewById((R.id.button8));
        button14 = findViewById((R.id.button14));
        button15 = findViewById((R.id.button15));
        button16 = findViewById((R.id.button16));
        button17 = findViewById((R.id.button17));
        button18 = findViewById((R.id.button18));
        button19 = findViewById((R.id.button19));
        button20 = findViewById((R.id.button20));
        button22 = findViewById((R.id.button22));

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent yeniIntent = new Intent(MainActivity3.this, MainActivity4.class);
                startActivity(yeniIntent);
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent yeniIntent = new Intent(MainActivity3.this, MainActivity5.class);
                startActivity(yeniIntent);
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent yeniIntent = new Intent(MainActivity3.this, MainActivity6Game.class);
                startActivity(yeniIntent);
            }
        });

        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent yeniIntent = new Intent(MainActivity3.this, MainActivity6.class);
                startActivity(yeniIntent);
            }
        });

        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent yeniIntent = new Intent(MainActivity3.this, MainActivity7.class);
                startActivity(yeniIntent);
            }
        });

        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent yeniIntent = new Intent(MainActivity3.this, MainActivity8.class);
                startActivity(yeniIntent);
            }
        });

        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent yeniIntent = new Intent(MainActivity3.this, MainActivity9.class);
                startActivity(yeniIntent);
            }
        });

        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent yeniIntent = new Intent(MainActivity3.this, MainActivity10.class);
                startActivity(yeniIntent);
            }
        });

        button15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent yeniIntent = new Intent(MainActivity3.this, MainActivity11.class);
                startActivity(yeniIntent);
            }
        });

        button14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent yeniIntent = new Intent(MainActivity3.this, MainActivity.class);
                startActivity(yeniIntent);
            }
        });

        button16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent yeniIntent = new Intent(MainActivity3.this, MainActivity2.class);
                startActivity(yeniIntent);
            }
        });

        button17.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent yeniIntent = new Intent(MainActivity3.this, MainActivity12.class);
                startActivity(yeniIntent);
            }
        });

        button18.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent yeniIntent = new Intent(MainActivity3.this, MainActivity13.class);
                startActivity(yeniIntent);
            }
        });
        button19.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent yeniIntent = new Intent(MainActivity3.this, MainActivity14.class);
                startActivity(yeniIntent);
            }
        });

        button20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent yeniIntent = new Intent(MainActivity3.this, MainActivity15.class);
                startActivity(yeniIntent);
            }
        });

        button22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent yeniIntent = new Intent(MainActivity3.this, MainActivity16.class);
                startActivity(yeniIntent);
            }
        });

    }
}