package com.example.tryings;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

public class MainActivity9 extends AppCompatActivity {

    private FloatingActionButton fab;
    private TextInputEditText txtInputAd, txtInputSoyad;
    private Button button12;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main9);

        fab = findViewById(R.id.fab);
        txtInputAd = findViewById(R.id.txtInputAd);
        txtInputSoyad = findViewById(R.id.txtInputSoyad);
        button12 = findViewById(R.id.button12);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(),"Merhaba",Toast.LENGTH_SHORT).show();
            }
        });

        button12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ad = txtInputAd.getText().toString();
                String soyad = txtInputSoyad.getText().toString();
                Toast.makeText(getApplicationContext(),"Ad: "+ad+" Soyad: "+soyad,Toast.LENGTH_SHORT).show();
            }
        });
    }
}