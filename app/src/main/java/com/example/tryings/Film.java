package com.example.tryings;

public class Film {
    private int filmID;
    private String filmAdi;
    private String filmResimAdi;
    private double filmFiyat;

    public Film() {
    }

    public Film(int filmID, String filmAdi, String filmResimAdi, double filmFiyat) {
        this.filmID = filmID;
        this.filmAdi = filmAdi;
        this.filmResimAdi = filmResimAdi;
        this.filmFiyat = filmFiyat;
    }

    public int getFilmID() {
        return filmID;
    }

    public void setFilmID(int filmID) {
        this.filmID = filmID;
    }

    public String getFilmAdi() {
        return filmAdi;
    }

    public void setFilmAdi(String filmAdi) {
        this.filmAdi = filmAdi;
    }

    public String getFilmResimAdi() {
        return filmResimAdi;
    }

    public void setFilmResimAdi(String filmResimAdi) {
        this.filmResimAdi = filmResimAdi;
    }

    public double getFilmFiyat() {
        return filmFiyat;
    }

    public void setFilmFiyat(double filmFiyat) {
        this.filmFiyat = filmFiyat;
    }
}
