package com.example.tryings;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class FilmAdapter extends RecyclerView.Adapter<FilmAdapter.CardViewHolder>{

    private Context context;
    private List<Film> filmList;

    public FilmAdapter(Context context, List<Film> filmListe) {
        this.context = context;
        this.filmList = filmListe;
    }

    public class CardViewHolder  extends RecyclerView.ViewHolder
    {
        public ImageView ivFilmResim;
        public TextView tvFilmAd, tvFiyat;
        public Button btnSepeteEkle;


        public CardViewHolder(@NonNull View itemView) {
            //Bu kod görsel nesnelerimizi temsil ediyor.
            super(itemView);
            ivFilmResim = itemView.findViewById(R.id.ivFilmResim);
            tvFilmAd = itemView.findViewById(R.id.tvFilmAd);
            tvFiyat = itemView.findViewById(R.id.tvFiyat);
            btnSepeteEkle = itemView.findViewById(R.id.btnSepeteEkle);

        }
    }

    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //Bu kod tasarımımızı temsil ediyor.
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardviewlayout,parent,false);
        return new CardViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewHolder holder, int position) {
            final Film film = filmList.get(position);
            holder.tvFiyat.setText(film.getFilmFiyat()+" TL");
            holder.tvFilmAd.setText(film.getFilmAdi());
            holder.ivFilmResim.setImageResource(context.getResources().getIdentifier(film.getFilmResimAdi(),"drawable",context.getPackageName()));
            holder.btnSepeteEkle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context,film.getFilmAdi()+" sepete eklendi.", Toast.LENGTH_SHORT).show();
                }
            });
    }

    @Override
    public int getItemCount() {
        return filmList.size();
    }
}
