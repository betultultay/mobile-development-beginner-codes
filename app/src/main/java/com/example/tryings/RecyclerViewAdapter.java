package com.example.tryings;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.CardViewTasarimTutucu>{
    private Context mContext;
    private List<String> ulkeler;

    public RecyclerViewAdapter(Context mContext, List<String> ulkeler) {
        this.mContext = mContext;
        this.ulkeler = ulkeler;
    }


    public class CardViewTasarimTutucu extends RecyclerView.ViewHolder
    {
        public TextView tvCardView;
        public CardView cardView;
        private ImageView more;

        public CardViewTasarimTutucu(View view)
        {
            super(view);
            tvCardView = view.findViewById(R.id.tvCardView);
            cardView = view.findViewById(R.id.cardView);
            more = view.findViewById(R.id.more);
        }
    }

    @NonNull
    @Override
    public CardViewTasarimTutucu onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardtasarim,parent,false);
        return new CardViewTasarimTutucu(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewTasarimTutucu holder, int position) {
        final String ulke = ulkeler.get(position);
        holder.tvCardView.setText(ulke);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, "Seçilen ülke: "+ulke, Toast.LENGTH_SHORT).show();
            }
        });

        holder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(mContext, view);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.cardmenu, popup.getMenu());
                popup.show();

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId())
                        {
                            case R.id.menusil:
                                Toast.makeText(mContext, "Sil tıklandı",Toast.LENGTH_SHORT).show();
                                return true;
                            case R.id.menuduzenle:
                                Toast.makeText(mContext, "Düzenle tıklandı",Toast.LENGTH_SHORT).show();
                                return true;
                            default:
                                return false;
                        }
                        
                    }
                });

            }
        });
    }



    @Override
    public int getItemCount() {
        //Burada belirttiğimiz sayı kadar onBindViewHolder metodu çalışacak.
        return ulkeler.size();
    }

}
