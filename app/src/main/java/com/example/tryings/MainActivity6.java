package com.example.tryings;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity6 extends AppCompatActivity {

    private Button button10, button11, button13;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main6);

        button10 = findViewById(R.id.button10);
        button11 = findViewById(R.id.button11);
        button13 = findViewById(R.id.button13);

        button10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity6.this,"Bu normal bir toast mesajıdır.",Toast.LENGTH_SHORT).show();
            }
        });

        button11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View tasarim = getLayoutInflater().inflate(R.layout.toast_tasarim,null);
                TextView textViewToast = tasarim.findViewById(R.id.textViewToast);
                textViewToast.setText("Bu özelleştirilmiş bir toast mesajıdır.");
                Toast ozelMesaj = new Toast(getApplicationContext());
                ozelMesaj.setView(tasarim);
                ozelMesaj.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL,0, 0);
                ozelMesaj.setDuration(Toast.LENGTH_SHORT);
                ozelMesaj.show();
            }
        });

        button13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PopupMenu popUp = new PopupMenu(MainActivity6.this,button13);
                popUp.getMenuInflater().inflate(R.menu.popupmenu,popUp.getMenu());
                popUp.show();
                popUp.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()){
                            case R.id.sil:
                                Toast.makeText(getApplicationContext(),"Sil seçildi.", Toast.LENGTH_SHORT).show();
                                return true;

                            case R.id.duzenle:
                                Toast.makeText(getApplicationContext(),"Düzenle seçildi.", Toast.LENGTH_SHORT).show();
                                return true;

                            default:
                                return false;
                        }

                    }
                });
            }
        });


    }
}