package com.example.tryings;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity12 extends AppCompatActivity {

    private RecyclerView rv;
    private ArrayList<String> ulkelerList;
    private RecyclerViewAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main12);

        rv = findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));
        ulkelerList = new ArrayList<String>();
        ulkelerList.add("Türkiye");
        ulkelerList.add("Çeçenistan");
        ulkelerList.add("Amerika");
        ulkelerList.add("Ukrayna");
        ulkelerList.add("İspanya");
        adapter = new RecyclerViewAdapter(getApplicationContext(),ulkelerList);
        rv.setAdapter(adapter);


    }
}