package com.example.tryings;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity4 extends AppCompatActivity {

    private EditText txtTarih;
    private EditText txtSaat;
    private ListView listView;
    private ArrayList<String> ulkeler = new ArrayList<>();
    private ArrayAdapter<String> veriAdaptoru;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);

        //Widgetları bağlama
        txtTarih = findViewById(R.id.txtTarih);
        txtSaat = findViewById(R.id.txtSaat);
        listView = findViewById(R.id.listView);

        //Arraye eleman ekleme
        ulkeler.add("Türkiye");
        ulkeler.add("Amerika");
        ulkeler.add("İspanya");
        ulkeler.add("İtalya");
        ulkeler.add("Hindistan");
        ulkeler.add("Güney Kore");
        ulkeler.add("Filipinler");
        ulkeler.add("Endonezya");
        ulkeler.add("Almanya");
        ulkeler.add("Brezilya");

        veriAdaptoru = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,android.R.id.text1,ulkeler);
        listView.setAdapter(veriAdaptoru);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getApplicationContext(),
                        "Seçilen ülke "+ulkeler.get(i),
                         Toast.LENGTH_SHORT).show();
            }
        });

        txtSaat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                int saat = calendar.get(Calendar.HOUR_OF_DAY);
                int dakika = calendar.get(Calendar.MINUTE);
                TimePickerDialog timePicker;

                timePicker = new TimePickerDialog(MainActivity4.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int i, int i1) {
                        txtSaat.setText(i+" : "+i1);
                    }
                },saat,dakika,true);

                timePicker.setButton(DialogInterface.BUTTON_POSITIVE, "Ayarla", timePicker);
                timePicker.setButton(DialogInterface.BUTTON_NEGATIVE, "İptal", timePicker);
                timePicker.show();
            }
        });

        txtTarih.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                int yil = calendar.get(Calendar.YEAR);
                int ay = calendar.get(Calendar.MONTH);
                int gun = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePicker;

                datePicker = new DatePickerDialog(MainActivity4.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        txtTarih.setText(i2+" . "+i1+" . "+i);
                    }
                },yil,ay,gun);

                datePicker.setButton(DialogInterface.BUTTON_POSITIVE, "Ayarla", datePicker);
                datePicker.setButton(DialogInterface.BUTTON_NEGATIVE, "İptal", datePicker);
                datePicker.show();
            }
        });
    }
}